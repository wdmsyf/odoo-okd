FROM ubuntu:focal
MAINTAINER NUTIM <nutim@guu.men>

SHELL ["/bin/bash", "-xo", "pipefail", "-c"]

# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8

# Install some deps, lessc and less-plugin-clean-css, and wkhtmltopdf
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    postgresql-client \
    python3-pip \
    python3-grpcio \
    python3-setuptools \
    xz-utils

# Install more pip
RUN pip3 install num2words xlwt firebase-admin ebaysdk 

# Install deps
COPY deps /opt/deps
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    /opt/deps/wkhtmltox_0.12.5-1.focal_amd64.deb \
    /opt/deps/odoo_13.0.20200530_all.deb

# Clean
RUN rm -rf /var/lib/apt/lists/* /opt/deps

# Copy entrypoint script and Odoo configuration file
COPY wait-for-psql.py /usr/local/bin/wait-for-psql.py
COPY odoo /etc/odoo
COPY addons /etc/odoo/addons
RUN chown odoo:0 /etc/odoo/odoo.conf \
        && chgrp -R 0 /etc/odoo/addons \
        && chmod -R g=u /etc/odoo/addons \
        && chgrp -R 0 /var/lib/odoo \
        && chmod -R g=u /var/lib/odoo \
        && chmod g=u /etc/passwd

# Mount /var/lib/odoo to allow restoring filestore and /mnt/extra-addons for users addons
RUN mkdir -p /mnt/extra-addons \
        && chown -R odoo:0 /mnt/extra-addons \
        && chmod -R g=u /mnt/extra-addons
VOLUME ["/var/lib/odoo", "/mnt/extra-addons"]

# Expose Odoo services
EXPOSE 8069 8071 8072

# Set the default config file
ENV ODOO_RC /etc/odoo/odoo.conf

# Set default user when running the container
USER 100001

ENTRYPOINT ["/etc/odoo/bin/uid_entrypoint"]

CMD ["/etc/odoo/bin/entrypoint.sh","odoo"]
