# odoo-okd

## build
```bash
sudo docker build -t nuttim/nuttim:13.0 .
#sudo docker tag cc963b2f8e9a nuttim/nuttim:13.0
sudo docker login
sudo docker push nuttim/nuttim:13.0
```
## 
```bash
sudo docker export 31f83bc129fe | sudo docker import - nuttim/nuttim:latest

# save the image to a tarball
docker save <IMAGE NAME> > /home/save.tar
 
# load it back
docker load < /home/save.tar


# export the container to a tarball
docker export <CONTAINER ID> > /home/export.tar
 
# import it back
cat /home/export.tar | docker import - some-name:latest

```

## test
```bash
./oc new-project odoo && ./oc create -f okd/odoo-postgres.yaml
```

