# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from dateutil import relativedelta
import datetime

from odoo import api, exceptions, fields, models, _


class MrpWorkcenter(models.Model):
    _inherit = 'mrp.workcenter'

    bs_partner_id = fields.Many2one('res.partner',domain=([('supplier', '=',
                                                            True)]),
                                    string="Partner")
    bs_product_id = fields.Many2one('product.product', domain=([('type','=',
                                                                 'service')]),
        string="Product")
    bs_cost = fields.Float('Cost per Qty')
    bs_picking_type_id = fields.Many2one('stock.picking.type',string="Picking Type")
    bs_picking_type_return_id = fields.Many2one('stock.picking.type',
                                           string="Return Type")

class MrpProductionWorkcenterLine(models.Model):
    _inherit = 'mrp.workorder'
    bs_purchase_id = fields.Many2one('purchase.order',string='Purchase',
                                     readonly=1)
    bs_delivery_id = fields.Many2one('stock.picking', string='Delivery',
                                     readonly=1)
    bs_return_id = fields.Many2one('stock.picking', string='Return',
                                   readonly=1)
    is_subcontract=fields.Boolean("Is Subcontract")


    @api.multi
    def create_subcontracting(self):
        for rec in self:
            product_id=rec.product_id
            quantity=rec.qty_producing
            workcenter_id=rec.workcenter_id
            production_id=rec.production_id
            partner_id=rec.workcenter_id and rec.workcenter_id.bs_partner_id
            purchase_product_id=rec.workcenter_id and rec.workcenter_id.bs_product_id
            picking_type_id=rec.workcenter_id and rec.workcenter_id.bs_picking_type_id
            picking_type_return_id=rec.workcenter_id and rec.workcenter_id.bs_picking_type_return_id
            bs_cost=rec.workcenter_id and rec.workcenter_id.bs_cost
            data=rec.get_delivery_return_product()
            delivery_product_id=data.get("delivery_product_id")
            return_product_id=data.get("return_product_id")
            last_product=data.get("last_product")
            raw_product_quantity=data.get("product_quantity")
            #delivery_product_id,return_product_id,last_product=rec.get_delivery_return_product()
            if last_product==True:
                return_product_id=product_id 
            
            if partner_id:
                location_dest_id=partner_id.property_stock_customer 
            if picking_type_id:
                location_id=picking_type_id.default_location_src_id
                #location_dest_id=picking_type_id.default_location_dest_id
            
                picking_obj=self.env["stock.picking"]
                stock_move_obj=self.env["stock.move"]
                purchase_obj=self.env["purchase.order"]
                purchase_line_obj=self.env["purchase.order.line"]
                
                ## Delivery Order Creation 
                picking_dictionary=({
                            
                            "partner_id":partner_id.id,
                            "picking_type_id":picking_type_id.id,
                            'location_id':location_id.id,
                            'location_dest_id':location_dest_id.id,
                            'company_id':1
                    })
                
                picking_id=picking_obj.create(picking_dictionary)
                if picking_id:
                    rec.bs_delivery_id=picking_id.id
                    stock_move_dictionary=({
                            
                            "product_id":delivery_product_id.id,
                            "picking_type_id":picking_type_id.id,
                            'location_id':location_id.id,
                            'location_dest_id':location_dest_id.id,
                            'product_uom_qty':raw_product_quantity,
                            'company_id':1,
                            'picking_id':picking_id.id,
                            'name':product_id.name,
                            'product_uom':product_id.uom_id.id
                    })
                    
                    stock_move_obj.create(stock_move_dictionary)
                    
                    ## Return Order Creation
            if picking_type_return_id:
                picking_dictionary=({
                        
                        "partner_id":partner_id.id,
                        "picking_type_id":picking_type_return_id.id,
                        'location_dest_id':location_id.id,
                        'location_id':location_dest_id.id,
                        'company_id':1
                })
                
                return_picking_id=picking_obj.create(picking_dictionary)
                if return_picking_id:
                    rec.bs_return_id=return_picking_id.id
                    stock_move_dictionary=({
                            
                            "product_id":return_product_id.id,
                            "picking_type_id":picking_type_id.id,
                            'location_dest_id':location_id.id,
                            'location_id':location_dest_id.id,
                            'product_uom_qty':raw_product_quantity,
                            'company_id':1,
                            'picking_id':return_picking_id.id,
                            'name':product_id.name,
                            'product_uom':product_id.uom_id.id
                    })
                    
                    stock_move_obj.create(stock_move_dictionary)

                    
                    ## Purchase Order Creation
                    purchase_dictionary=({
                                
                                "partner_id":partner_id.id,
                                "picking_type_id":picking_type_return_id.id,
                                "date_order":datetime.datetime.now(),
                                'currency_id':self.env.user.company_id.currency_id.id,
                                'location_dest_id':location_id
                                
                        })
                    purchase_id=purchase_obj.create(purchase_dictionary)
                    if purchase_id:
                        rec.bs_purchase_id=purchase_id.id
                        order_line_dictionary=({
                                "product_id":purchase_product_id.id,
                                'product_qty':quantity,
                                'name':product_id.name,
                                'product_uom':product_id.uom_id.id,
                                "date_planned":datetime.datetime.now(),
                                "price_unit":bs_cost,
                                'order_id':purchase_id.id
                                
                        })
                            
                        purchase_line_obj.create(order_line_dictionary)
            rec.state='progress'
            rec.is_subcontract=True
                
     
    @api.multi
    def get_delivery_return_product(self):
        obj=self.env["mrp.bom.line"]
        last_product_id=''
        product_id=''
        last_product=False
        return_product_id=''
        product_quantity=0.0
        for rec in self:
            production_id=rec.production_id
            if production_id:
                bom_id=production_id.bom_id
                if bom_id:
                    line_ids=obj.search([('operation_id','=',self.operation_id.id),('bom_id','=',bom_id.id)])
                    last_lines=obj.search([('bom_id','=',bom_id.id)],order='id desc',limit=1)
                    for line_id in line_ids:
                        product_id=line_id.product_id
                        product_quantity=line_id.product_qty
#                        if product_id:
#                            return product_id
                    for last_line in last_lines:
                        last_product_id=last_line.product_id
                        if last_product_id:
                            if last_product_id.id == product_id.id:
                                last_product=True
                            else:
                                for line_id in line_ids:
                                    next_line_id=line_id.id+1
                                    next_lines=obj.browse(next_line_id)
                                    for next_line in next_lines:
                                        return_product_id=next_line.product_id
                                        product_quantity=next_line.product_qty
                    if product_id:
                        return {'product_quantity':product_quantity,'delivery_product_id':product_id,'return_product_id':return_product_id,'last_product':last_product}
    
    
        
            
class ProductTemplate(models.Model):
    _inherit = "product.template"
    
    bs_composition = fields.Many2one('product.composition',string='Composition')
    bs_brand = fields.Many2one('product.brand', string='Product Brand')
    denier = fields.Float('Denier')
    filament = fields.Float('Filament')
    play = fields.Float('Play')
    luster = fields.Char('Luster')
    
    
class ProductComposition(models.Model):
    _name = "product.composition"
            
    name = fields.Char('Composition',required=True)
    
class ProductBrand(models.Model):
    _name = "product.brand"
            
    name = fields.Char('Product Brand',required=True)        
            
            

