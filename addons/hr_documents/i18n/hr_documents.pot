# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* hr_documents
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~12.4+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-12 11:29+0000\n"
"PO-Revision-Date: 2019-08-12 11:29+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: hr_documents
#: model:ir.model.fields,field_description:hr_documents.field_res_users__document_ids
msgid "Document"
msgstr ""

#. module: hr_documents
#: model:ir.model.fields,field_description:hr_documents.field_hr_employee__document_count
#: model:ir.model.fields,field_description:hr_documents.field_res_users__document_count
msgid "Documents"
msgstr ""

#. module: hr_documents
#: model:ir.model,name:hr_documents.model_hr_employee
msgid "Employee"
msgstr ""

#. module: hr_documents
#: model:ir.model,name:hr_documents.model_res_users
msgid "Users"
msgstr ""
