kind: Template
apiVersion: v1
metadata:
  name: postgresql-persistent
  annotations:
    openshift.io/display-name: PostgreSQL
    description: |-
      PostgreSQL database service, with persistent storage. For more information about using this template, including OpenShift considerations, see https://github.com/sclorg/postgresql-container/.

      NOTE: Scaling to more than one replica is not supported. You must have persistent volumes available in your cluster to use this template.
    iconClass: icon-postgresql
    tags: database,postgresql
    openshift.io/long-description: This template provides a standalone PostgreSQL
      server with a database created.  The database is stored on persistent storage.  The
      database name, username, and password are chosen via parameters when provisioning
      this service.
    openshift.io/provider-display-name: Red Hat, Inc.
    openshift.io/documentation-url: https://docs.okd.io/latest/using_images/db_images/postgresql.html
    openshift.io/support-url: https://access.redhat.com
message: |-
  The following service(s) have been created in your project: ${DATABASE_SERVICE_NAME}.

         Username: ${POSTGRESQL_USER}
         Password: ${POSTGRESQL_PASSWORD}
    Database Name: ${POSTGRESQL_DATABASE}
   Connection URL: postgresql://${DATABASE_SERVICE_NAME}:5432/

  For more information about using this template, including OpenShift considerations, see https://github.com/sclorg/postgresql-container/.
labels:
  template: postgresql-persistent-template
objects:
###start db
##Secret
- kind: Secret
  apiVersion: v1
  metadata:
    name: "${DATABASE_SERVICE_NAME}"
    annotations:
      template.openshift.io/expose-username: "{.data['database-user']}"
      template.openshift.io/expose-password: "{.data['database-password']}"
      template.openshift.io/expose-database_name: "{.data['database-name']}"
  stringData:
    database-user: "${POSTGRESQL_USER}"
    database-password: "${POSTGRESQL_PASSWORD}"
    database-name: "${POSTGRESQL_DATABASE}"
##Service
- kind: Service
  apiVersion: v1
  metadata:
    name: "${DATABASE_SERVICE_NAME}"
    annotations:
      template.openshift.io/expose-uri: postgres://{.spec.clusterIP}:{.spec.ports[?(.name=="postgresql")].port}
  spec:
    ports:
    - name: postgresql
      protocol: TCP
      port: 5432
      targetPort: 5432
      nodePort: 0
    selector:
      name: "${DATABASE_SERVICE_NAME}"
    type: ClusterIP
    sessionAffinity: None
  status:
    loadBalancer: {}
##PersistentVolumeClaim
- kind: PersistentVolumeClaim
  apiVersion: v1
  metadata:
    name: "pvc-${DATABASE_SERVICE_NAME}"
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: "${VOLUME_CAPACITY}"
##DeploymentConfig
- kind: DeploymentConfig
  apiVersion: v1
  metadata:
    name: "${DATABASE_SERVICE_NAME}"
    annotations:
      template.alpha.openshift.io/wait-for-ready: 'true'
  spec:
    strategy:
      type: Recreate
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - postgresql
        from:
          kind: ImageStreamTag
          name: postgresql:${POSTGRESQL_VERSION}
          namespace: openshift
        lastTriggeredImage: ''
    - type: ConfigChange
    replicas: 1
    selector:
      name: "${DATABASE_SERVICE_NAME}"
    template:
      metadata:
        labels:
          name: "${DATABASE_SERVICE_NAME}"
      spec:
        containers:
        - name: postgresql
          image: " "
          ports:
          - containerPort: 5432
            protocol: TCP
          readinessProbe:
            timeoutSeconds: 1
            initialDelaySeconds: 5
            exec:
              command:
              - "/usr/libexec/check-container"
          livenessProbe:
            timeoutSeconds: 10
            initialDelaySeconds: 120
            exec:
              command:
              - "/usr/libexec/check-container"
              - "--live"
          env:
          - name: POSTGRESQL_USER
            valueFrom:
              secretKeyRef:
                name: "${DATABASE_SERVICE_NAME}"
                key: database-user
          - name: POSTGRESQL_PASSWORD
            valueFrom:
              secretKeyRef:
                name: "${DATABASE_SERVICE_NAME}"
                key: database-password
          - name: POSTGRESQL_DATABASE
            valueFrom:
              secretKeyRef:
                name: "${DATABASE_SERVICE_NAME}"
                key: database-name
          resources:
            limits:
              memory: "${MEMORY_LIMIT}"
          volumeMounts:
          - name: "${DATABASE_SERVICE_NAME}-data"
            mountPath: "/var/lib/pgsql/data"
          terminationMessagePath: "/dev/termination-log"
          imagePullPolicy: IfNotPresent
          capabilities: {}
          securityContext:
            capabilities: {}
            privileged: false
        volumes:
        - name: "${DATABASE_SERVICE_NAME}-data"
          persistentVolumeClaim:
            claimName: "pvc-${DATABASE_SERVICE_NAME}"
        restartPolicy: Always
        dnsPolicy: ClusterFirst
  status: {}
###end db
parameters:
##db
- name: DATABASE_SERVICE_NAME
  displayName: Database Service Name
  description: The name of the OpenShift Service exposed for the database.
  value: db
  required: true
- name: POSTGRESQL_USER
  displayName: PostgreSQL Connection Username
  description: Username for PostgreSQL user that will be used for accessing the database.
  generate: expression
  from: user[A-Z0-9]{3}
  required: true
- name: POSTGRESQL_PASSWORD
  displayName: PostgreSQL Connection Password
  description: Password for the PostgreSQL connection user.
  generate: expression
  from: "[a-zA-Z0-9]{16}"
  required: true
- name: POSTGRESQL_DATABASE
  displayName: PostgreSQL Database Name
  description: Name of the PostgreSQL database accessed.
  value: db1
  required: true
- name: POSTGRESQL_VERSION
  displayName: Version of PostgreSQL Image
  description: Version of PostgreSQL image to be used (10 or latest).
  value: '12'
  required: true
- name: VOLUME_CAPACITY
  displayName: Volume Capacity
  description: Volume space available for data, e.g. 2Gi, 3Gi, 5Gi.
  value: 5Gi
  required: true
- name: MEMORY_LIMIT
  displayName: Memory Limit
  description: Maximum amount of memory the container can use.
  value: 512Mi
  required: true
